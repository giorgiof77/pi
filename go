#!/bin/bash

# Verify that nodejs is available, if not installed it
if which node > /dev/null
  then
  echo "Node Is Available"
else
  echo "Node needs to be installed" 
  curl -sL deb.nodesource.com/setup_4.x | sudo sh
  sudo apt-get install -y nodejs
fi 

node scripts.js