/******************************************************
* Digital Menu Boards Copyrigths 2018
* 
* To install the application run the following command
*    $ sudo node install.js
*
******************************************************/


const cmd = require('child_process').execSync;
const path = require('path');
const fs = require('fs');

var opt = {encoding:'utf8'};
var autostartPath1 = "/home/pi/.config/lxsession/LXDE-pi/autostart";
var autostartPath2 = "/etc/xdg/lxsession/LXDE-pi/autostart";
var autostartPath;
// first get the current working dir
var startDir = cmd('pwd',opt);
/***
at this directory there are the following files
<startDir>/
  |--dmenuPI.zip
  |--startdmenu.sh
  |--install.sh
  |--README.md
  |--scripts.js
  |--checkUpdates.sh
*/

//Create the following folder structure
  console.log('\n#############################################################');
	console.log('########### ########### ###########             #############');
	console.log('########### ########### ###########             #############');
	console.log('########### ########### ###########             #############');
	console.log('########### ########### ###########             #############');
	console.log('########### ########### ###########             #############');
	console.log('########### ########### ###########             #############');
	console.log('########### =================================== #############');
	console.log('###########                                     #############');
	console.log('###########                                     #############');
	console.log('###########         www.dmenuboard.com          #############');
	console.log('###########                                     #############');
    console.log('###########                                     #############');
    console.log('#############################################################');
    console.log('########### Digital Menu Board, 2018 Copyrights #############');
    console.log('#############################################################');
    console.log('#############################################################\n\n\n');
console.log('Dmenuboard Installer Started...');
console.log('I am going to take 9 steps; kick back and relax, should only take about 5 minutes to get this ready!');
console.log('Nothing else you have to do now, once done I will be rebooting and start working!');



var install = function (){
	
	console.log('1/9 creating folder structure');
	cmd('mkdir -p ~/DMENU');

	console.log('2/9 Make startdmenu.sh file executable');
	cmd('chmod 775 startdmenu.sh');
	console.log('Done!');

	console.log('3/9 Copy startdmenu.sh file to DMENU/');
	cmd('cp startdmenu.sh ~/DMENU/');
	console.log('Done!');

	//console.log('4/9 Copy the dmenuPI.zip into ~/DMENU/');
	//cmd('cp dmenuPI.zip ~/DMENU/');
	//console.log('Done!');

	console.log('4/9 Unzip the application');
	var output = cmd(' unzip -o dmenuPI.zip -d ~/DMENU',opt);
	console.log(output);
	console.log('Done!');

	console.log('5/9 Make the dmanuPI executable');
	cmd('chmod 775 ~/DMENU/dmenuPI');
	console.log('Done!');

	// console.log('7a/9 Copy Application files into /DMENU/dmanuapp/');
	// cmd('cp -a temp/dmenuapp. ~/DMENU/dmenuapp');
	// console.log('Done!');

	// console.log('7b/9 Cleaning Temp Folder');
	// cmd('rm -rf ./temp');
	// console.log('Done!');
	var autoStartFile;
	var fileExists=false;
	if(fs.existsSync(autostartPath2)){
		autoStartFile = fs.readFileSync(autostartPath2,opt);
		autostartPath = autostartPath2;
		
		fileExists = true;
	}
	if(fs.existsSync(autostartPath1)){
		autoStartFile = fs.readFileSync(autostartPath1,opt);
		autostartPath = autostartPath1;
		fileExists = true;
	}
	console.log('autostartPath:',autostartPath);
	if(fileExists){
		console.log('Current autostart file:\n'+autoStartFile);

		if(!autoStartFile.includes('@xset s0 0')){
			console.log('6a/11 Disabling screensaver');
			autoStartFile = autoStartFile+'@xset s 0 0\n@xset s noblank\n@xset s noexpose\n@xset dpms 0 0 0\n'
		}
		//var lines = autoStartFile.split('\n');

		if(!autoStartFile.includes('@sh /home/pi/DMENU/startdmenu.sh')){
			console.log('6b/11 Adding startdmenuapp.sh at boot');
			autoStartFile = autoStartFile+'@sh /home/pi/DMENU/startdmenu.sh\n';
		}
				
		console.log('7/9 Saving autostart file');
		cmd('sudo chmod 777 '+autostartPath);
		fs.writeFileSync(autostartPath, autoStartFile, 'utf8');
		console.log('New autostart file:\n'+autoStartFile);
	    cmd('sudo chmod 655 '+autostartPath);
	    console.log('Done!');
	}


	// console.log('10/9 Cleaning installation directory');
	// cmd('rm ~/DMENU/');
	// console.log('Done!');

	console.log('8/9 Disable Overscan, to fill the screen');
	cmd("sudo sed -i -e 's/#disable_overscan=1/disable_overscan=1/g' /boot/config.txt");
	console.log('Done!');

	

	console.log('Set Background');
	cmd("pcmanfm --set-wallpaper ~/DMENU/pi/dmenuboard-bg.jpg");
	console.log('Done!');
	
	console.log('9/10 Check if this is a Development Environment App ');
	if(fs.existsSync('./environment.json')){
		var envInfo = JSON.parse(fs.readFileSync('./environment.json'))
		console.log('Configuring for ');
		cmd('mkdir ~/DMENU/resources/data/')
		cmd('mv ./environment.json ~/DMENU/resources/data/environment.json')
	}else{
		console.log('This is Production! all good!')
	}
	cmd('cp ./env.prop ~/DMENU/env.prop')
	cmd("sudo sed -i -e 's/#disable_overscan=1/disable_overscan=1/g' /boot/config.txt");
	console.log('10/10 Reboot, All Done, I will restart now and will be up and running shortly!');

	setTimeout(reboot,2000);
}

var reboot = function reboot(){
	cmd('reboot');
}

setTimeout(install, 3000); // Start here after waiting for 3 seconds

// Clean this directory
// lounch dmenuPI


