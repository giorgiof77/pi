#!/bin/bash
echo "I started it!"
echo pwd

source ./env.prop
echo $ENV
echo $ENVREPO



# Test if the internet is available for about 15 seconds
NetworkAvailable="0"
i="0"
while [ $i -lt 16 ]
do
  echo "i:$i"
  echo -e "GET http://google.com HTTP/1.0\n\n" | nc google.com 80 > /dev/null 2>&1

  if [ $? -eq 0 ]; then
      echo "Online"
      NetworkAvailable=1
      break;
  else
      echo "Offline"
      sleep 1
  fi

  i=$[$i+1]
  echo "increased i to $i"
done

if [ "$NetworkAvailable" -eq 1 ]; then
  echo "Network is Up "
  if [[ -f ~/DMENU/resources/data/update.software.flag ]]; then
    # check for Sw Updates
    cd ~/DMENU/pi
    git fetch
    HEADHASH=$(git rev-parse HEAD)
    UPSTREAMHASH=$(git rev-parse master@{upstream})

    if [ "$HEADHASH" != "$UPSTREAMHASH" ]; then
      echo "downloading new software"
    fi # End of Software Update
      # New Software Available
      #set background to show software update
      pcmanfm --set-wallpaper ~/DMENU/pi/softwareUpdate.png
      # Check if there is the old folder for the SW Update
      if [[ -d ~/DMENU/NEWSW ]]; then
        #it should not be there
        sudo rm -rf ~/DMENU/NEWSW
      fi
      
      #create the newsw folder and enter in it
      mkdir ~/DMENU/NEWSW 
    
      # enter the folder and pull the latest files
      cd ~/DMENU/NEWSW
      git clone --depth=1 $ENVREPO
      #https://dmenuboard@bitbucket.org/dmenuboard/pi.git

      # pass control to the new software update executable
      # ensure it is executable
      chmod 755 ~/DMENU/NEWSW/pi/install_new_sw.sh
      nohup ~/DMENU/NEWSW/pi/install_new_sw.sh &

      # Exit this script
      exit 1
   
  fi # End of Software Flag Check
fi # End of Network Up

# Clean flag Files
if [[ -f ~/DMENU/NEWSW/new.sw.ready ]]; then
  rm ~/DMENU/NEWSW/new.sw.ready
fi

if [[ -f ~/DMENU/resources/data/new.sw.ready ]]; then
  rm ~/DMENU/resources/data/new.sw.ready
fi



# No software Update at this point or No Network Available
pcmanfm --set-wallpaper ~/DMENU/pi/dmenuboard-bg.jpg


if [ "$ENV" -eq "qa1"]; then
  cp ~/DMENU/pi/environment.json ~/DMENU/resources/data/environment.json
fi
# Start the Application
#### Start Application ####
cd ~/DMENU
pwd
./dmenuPI
  
  # Create the file that say new software is available
  # ~/DMENU/NEWSW/new.sw.available
  #touch ~/DMENU/NEWSW/new.sw.available
  
  #chmod 755 ~/DMENU/pi/download-new-sw.sh
  #touch new.sw.available
  #sh ~/DMENU/pi/download-new-sw.sh 
  # rm -rfv ~/DMENU/pi
  
  # git clone --depth=1 https://dmenuboard@bitbucket.org/dmenuboard/pi.git
  # Check if I already rebbot once from this point
#   if [[ ! -f ~/DMENU/rebooted.once ]]; then
#     touch ~/DMENU/rebooted.once
#     reboot
#   else
#     echo "something is wrong"
#   fi
#   #Enter the repo and start the software update
#   # cd ~/DMENU/pi
#   # node softwareUpdate.js
# else
#   echo "software up to date"
#   if [[ ! -f ~/DMENU/rebooted.once ]]; then
#     rm -f ~/DMENU/rebooted.once
#   fi  
# fi
  



